import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent {
  @Input() name = "Saikal";
  @Input() icon = "/assets/dbab4ay-1902be68-97d1-44d0-845f-21a9dced5edf.png";
  @Input() year = "1995";
}
